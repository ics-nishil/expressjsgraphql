const { Student } = require("./models/Student.js");

const root = {
  hello: () => { return "GraphQL is Awesome" },
  welcome: (params) => { return `Hello ${params.name}` },
  students: async () => await Student.find({}),
  student: async (args) => await Student.findById(args.id),

  create: async ({input}) => {
    
    try{
      const newStudent = new Student(input);
      await newStudent.save();
      return newStudent;
    }
    catch (error) {
      throw new Error("Failed to create student.");
    }
  },

  update: async ({id, input}) => {
    try {
      const result = await Student.findByIdAndUpdate(id, input);
      return result;
    } catch (error) {
        throw new Error("Failed to update student details.");
    }
  },

  delete: async({id}) => {
    try {
      const result = await Student.findByIdAndDelete(id);
      return result;
    } catch (error) {
        throw new Error("Failed to delete student.");
    }
  }

};

module.exports = root;