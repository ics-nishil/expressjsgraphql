const { buildSchema } = require("graphql");

const schema = buildSchema(`
  type Query {
    hello: String
    welcome(name: String): String
    students: [Student]  
    student(id: ID): Student 
  }

  type Student {
    id: ID
    firstName: String
    lastName: String
    age: Int
  }

  input userInput{
    firstName: String, 
    lastName: String, 
    age: Int
  }

  type Mutation {
    create(input: userInput!): Student
    update(id: ID!, input: userInput): Student
    delete(id: ID!): Student
  }
  
`);

module.exports = schema;