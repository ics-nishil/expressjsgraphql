const express = require("express");
const mongoose = require("mongoose");
const { graphqlHTTP } = require('express-graphql');

const schema = require('./models/schema');
const root = require('./userRoot');


const app = express();

//Middleware
app.use(express.json());

const MONGO_URI = "mongodb+srv://nishilics:q4e5xY7CnHTHiinQ@graphql.nctnyzv.mongodb.net/GraphQL?retryWrites=true&w=majority";

mongoose
  .connect(MONGO_URI)
  .then(() => {
    console.log(`Database Connected..........`);
  })
  .catch(err => {
    console.log(err.message);
  });


app.use('/graphql', graphqlHTTP({
    graphiql: true,
    schema: schema,
    rootValue: root
}));

const PORT = 4000;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

